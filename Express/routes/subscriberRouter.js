const express = require('express');
const bodyParser = require('body-parser');
const subscriberRouter = express.Router();

subscriberRouter.use(bodyParser.json());
subscriberRouter.route('/')
    .all((req, res, next) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain');
        next();
    })
    .get((req, res, next) => {
        res.end('Will send all the subscribers to you!');
    })
    .post((req, res, next) => {
        res.end('Will add the subscriber: ' + req.body.name + ' with details: ' + req.body.description);
    })
    .put((req, res, next) => {
        res.statusCode = 403;
        res.end('PUT operation not supported on /subscribers');
    })
    .delete((req, res, next) => {
        res.end('Deleting all subscribers');
    });

subscriberRouter.route('/:subscriberId')
    .all((req, res, next) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain');
        next();
    })
    .get((req, res, next) => {
        res.end('Will send details of the subscriber: ' + req.params.subscriberId + ' to you!');
    })
    .post((req, res, next) => {
        res.statusCode = 403;
        res.end('POST operation not supported on /subscriber/' + req.params.subscriberId);
    })
    .put((req, res, next) => {
        res.write('Updating the subscriber: ' + req.params.subscriberId + '\n');
        res.end('Will update the subscriber: ' + req.body.name +
            ' with details: ' + req.body.description);
    })
    .delete((req, res, next) => {
        res.end('Deleting subscriber: ' + req.params.subscriberId);
    });

module.exports = subscriberRouter;