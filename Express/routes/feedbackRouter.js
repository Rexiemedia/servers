const express = require('express');
const bodyParser = require('body-parser');
const feedbackRouter = express.Router();

feedbackRouter.use(bodyParser.json());
feedbackRouter.route('/')
    .all((req, res, next) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain');
        next();
    })
    .get((req, res, next) => {
        res.end('Will send all the feedbacks to you!');
    })
    .post((req, res, next) => {
        res.end('Will add the feedback: ' + req.body.name + ' with details: ' + req.body.description);
    })
    .put((req, res, next) => {
        res.statusCode = 403;
        res.end('PUT operation not supported on /feedbacks');
    })
    .delete((req, res, next) => {
        res.end('Deleting all feedbacks');
    });

feedbackRouter.route('/:feedbackId')
    .all((req, res, next) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain');
        next();
    })
    .get((req, res, next) => {
        res.end('Will send details of the feedback: ' + req.params.feedbackId + ' to you!');
    })
    .post((req, res, next) => {
        res.statusCode = 403;
        res.end('POST operation not supported on /feedbacks/' + req.params.feedbackId);
    })
    .put((req, res, next) => {
        res.write('Updating the feedback: ' + req.params.feedbackId + '\n');
        res.end('Will update the feedback: ' + req.body.name +
            ' with details: ' + req.body.description);
    })
    .delete((req, res, next) => {
        res.end('Deleting feedback: ' + req.params.feedbackId);
    });

module.exports = feedbackRouter;