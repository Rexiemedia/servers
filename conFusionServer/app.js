var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
const session = require('express-session');
const FileStore = require('session-file-store')(session);
var passport = require('passport');
var authenticate = require('./authenticate');
var config = require('./config');

var uploadRouter = require('./routes/uploadRouter');
var index = require('./routes/index');
var users = require('./routes/users');
var dishRouter = require('./routes/dishRouter');
var promotionRouter = require('./routes/promotionRouter');
var feedbackRouter = require('./routes/feedbackRouter');
var leaderRouter = require('./routes/leaderRouter');
var subscriberRouter = require('./routes/subscriberRouter');

const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');

const Dishes = require('./models/dishes');
const Leaders = require('./models/leaders');
const Promotions = require('./models/promotions');
const Feedbacks = require('./models/feedbacks');
const Users = require('./models/user');
const Subscribers = require('./models/subscribers');

// Connection URL
const url = config.mongoUrl;
const connect = mongoose.connect(url, {
  useMongoClient: true,
  /* other options */
});

connect.then((db) => {
  console.log("Connected correctly to server");
}, (err) => { console.log(err); });

var app = express();
//redirect to secure port
app.all('*', (req, res, next) => {
  if(req.secure){
    return next();
  }
  else {
    res.redirect(307, 'https://'+ req.hostname + ':' + app.get('secPort') + req.url);
}
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// app.use(session({
//   name: 'session-id',
//   secret: '12345-67890-09876-54321',
//   saveUninitialized: false,
//   resave: false,
//   store: new FileStore()
// }));

app.use(passport.initialize());
// app.use(passport.session());

app.use('/', index);
app.use('/users', users);

app.use(express.static(path.join(__dirname, 'public')));

//dishes collection in the dishRouter end point
app.use('/imageUpload', uploadRouter);
app.use('/dishes', dishRouter);
app.use('/leaders', leaderRouter);
app.use('/promotions', promotionRouter);
app.use('/feedbacks', feedbackRouter);
//subscribers collection in the subscriberRouter end point
app.use('/subscribers', subscriberRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;

/*
// var express = require('express');
// var path = require('path');
// var favicon = require('serve-favicon');
// var logger = require('morgan');
// var cookieParser = require('cookie-parser');
// var bodyParser = require('body-parser');
// const mongoose = require('mongoose'); 
// mongoose.Promise = require('bluebird');
// const session = require('express-session');
// const FileStore = require('session-file-store')(session);
// var passport = require('passport');
// var authenticate = require('./authenticate');

// const Dishes = require('./models/dishes');
// const Leaders = require('./models/leaders');
// const Promotions = require('./models/promotions');
// const Feedbacks = require('./models/feedbacks');
// const Users = require('./models/user');
// const Subscribers = require('./models/subscribers');

// // Connection URL
// const url = 'mongodb://localhost:27017/conFusion';
// const connect = mongoose.connect(url, {
//   useMongoClient: true,
//   /* other options */
// });

// connect.then((db) => {
//   console.log("Connected correctly to server");
// }, (err) => { console.log(err); });

// var index = require('./routes/index');
// var users = require('./routes/users');
// var dishRouter = require('./routes/dishRouter');
// var promotionRouter = require('./routes/promotionRouter');
// var feedbackRouter = require('./routes/feedbackRouter');
// var leaderRouter = require('./routes/leaderRouter');
// var subscriberRouter = require('./routes/subscriberRouter');

// var app = express();

// // view engine setup
// app.set('views', path.join(__dirname, 'views'));
// app.set('view engine', 'jade');

// // uncomment after placing your favicon in /public
// //app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
// app.use(logger('dev'));
// app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({ extended: false }));
// //app.use(cookieParser('12345-67890-09876-54321'));

// app.use(session({
//   name: 'session-id',
//   secret: '12345-67890-09876-54321',
//   saveUninitialized: false,
//   resave: false,
//   store: new FileStore()
// }));

// app.use(passport.initialize());
// app.use(passport.session());

// app.use('/', index);
// app.use('/users', users);

// // function auth(req, res, next) {
// //   console.log(req.session);

// //   if (!req.session.user) {
// //     var err = new Error('You are not authenticated!');
// //     err.status = 403;
// //     return next(err);
// //   }
// //   else {
// //     if (req.session.user === 'authenticated') {
// //       next();
// //     }
// //     else {
// //       var err = new Error('You are not authenticated!');
// //       err.status = 403;
// //       return next(err);
// //     }
// //   }
// // }
// // app.use('/', index);
// // app.use('/users', users);

// // function auth(req, res, next) {
// //   // console.log(req.signedCookies);
// //   console.log(req.session);
// //   // if (!req.signedCookies.user) {
// //   if (!req.session.user) {
// //     //var authHeader = req.headers.authorization;
// //     //if (!authHeader) {
// //       var err = new Error('You are not authenticated!');
// //       res.setHeader('WWW-Authenticate', 'Basic');
// //       err.status = 403;
// //       next(err);
// //       return;
// //     }
// //     // var auth = new Buffer(authHeader.split(' ')[1], 'base64').toString().split(':');
// //     // var user = auth[0];
// //     // var pass = auth[1];
// //     // if (user == 'admin' && pass == 'password') {
// //     //   // res.cookie('user', 'admin', { signed: true });
// //     //   req.session.user = 'admin';
// //     //   next(); // authorized
// //   //  } 
// //     // else {
// //     //   var err = new Error('You are not authenticated!');
// //     //   res.setHeader('WWW-Authenticate', 'Basic');
// //     //   err.status = 401;
// //     //   return next(err);
// //     // }
// //   //}
// //   else {
// //     // if (req.signedCookies.user === 'admin') {
// //     if (req.session.user === 'authenticated') {
// //       next();
// //     }
// //     else {
// //       var err = new Error('You are not authenticated!');
// //       err.status = 403;
// //       next(err);
// //     }
// //   }
// //}
// app.use(auth);
// app.use(express.static(path.join(__dirname, 'public')));

// //dishes collection in the dishRouter end point
// app.use('/dishes', dishRouter);
// app.use('/leaders', leaderRouter);
// app.use('/promotions', promotionRouter);
// app.use('/feedbacks', feedbackRouter);
// //subscribers collection in the subscriberRouter end point
// app.use('/subscribers', subscriberRouter);

// // catch 404 and forward to error handler
// app.use(function (req, res, next) {
//   var err = new Error('Not Found');
//   err.status = 404;
//   next(err);
// });

// // error handler
// app.use(function (err, req, res, next) {
//   // set locals, only providing error in development
//   res.locals.message = err.message;
//   res.locals.error = req.app.get('env') === 'development' ? err : {};

//   // render the error page
//   res.status(err.status || 500);
//   res.render('error');
// });

// module.exports = app;
// */