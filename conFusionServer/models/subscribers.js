const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var subscriberSchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: false
    },
    email: {
        type: String,
        required: true
    }
}, {
        timestamps: true
    });

var Subscribers = mongoose.model('subscriber', subscriberSchema);

module.exports = Subscribers;