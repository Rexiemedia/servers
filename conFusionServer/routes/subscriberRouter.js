const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const Subscribers = require('../models/subscribers');

const subscriberRouter = express.Router();

subscriberRouter.use(bodyParser.json());

subscriberRouter.route('/')
    .get((req, res, next) => {
        Subscribers.find({})
            .then((subscribers) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(subscribers);
            }, (err) => next(err))
            .catch((err) => next(err));
    })
    .post((req, res, next) => {
        Subscribers.create(req.body)
            .then((subscriber) => {
                console.log('Dish Created ', subscriber);
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(subscriber);
            }, (err) => next(err))
            .catch((err) => next(err));
    })
    .put((req, res, next) => {
        res.statusCode = 403;
        res.end('PUT operation not supported on /subscribers');
    })
    .delete((req, res, next) => {
        Subscribers.remove({})
            .then((resp) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(resp);
            }, (err) => next(err))
            .catch((err) => next(err));
    });

subscriberRouter.route('/:subscriberId')
    .get((req, res, next) => {
        Subscribers.findById(req.params.subscriberId)
            .then((subscriber) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(subscriber);
            }, (err) => next(err))
            .catch((err) => next(err));
    })
    .post((req, res, next) => {
        res.statusCode = 403;
        res.end('POST operation not supported on /subscribers/' + req.params.subscriberId);
    })
    .put((req, res, next) => {
        Subscribers.findByIdAndUpdate(req.params.subscriberId, {
            $set: req.body
        }, { new: true })
            .then((subscriber) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(subscriber);
            }, (err) => next(err))
            .catch((err) => next(err));
    })
    .delete((req, res, next) => {
        Subscribers.findByIdAndRemove(req.params.subscriberId)
            .then((resp) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(resp);
            }, (err) => next(err))
            .catch((err) => next(err));
    });

module.exports = subscriberRouter;


// const express = require('express');
// const bodyParser = require('body-parser');
// const subscriberRouter = express.Router();

// subscriberRouter.use(bodyParser.json());
// subscriberRouter.route('/')
//     .all((req, res, next) => {
//         res.statusCode = 200;
//         res.setHeader('Content-Type', 'text/plain');
//         next();
//     })
//     .get((req, res, next) => {
//         res.end('Will send all the subscribers to you!');
//     })
//     .post((req, res, next) => {
//         res.end('Will add the subscriber: ' + req.body.name + ' with details: ' + req.body.description);
//     })
//     .put((req, res, next) => {
//         res.statusCode = 403;
//         res.end('PUT operation not supported on /subscribers');
//     })
//     .delete((req, res, next) => {
//         res.end('Deleting all subscribers');
//     });

// subscriberRouter.route('/:subscriberId')
//     .all((req, res, next) => {
//         res.statusCode = 200;
//         res.setHeader('Content-Type', 'text/plain');
//         next();
//     })
//     .get((req, res, next) => {
//         res.end('Will send details of the subscriber: ' + req.params.subscriberId + ' to you!');
//     })
//     .post((req, res, next) => {
//         res.statusCode = 403;
//         res.end('POST operation not supported on /subscriber/' + req.params.subscriberId);
//     })
//     .put((req, res, next) => {
//         res.write('Updating the subscriber: ' + req.params.subscriberId + '\n');
//         res.end('Will update the subscriber: ' + req.body.name +
//             ' with details: ' + req.body.description);
//     })
//     .delete((req, res, next) => {
//         res.end('Deleting subscriber: ' + req.params.subscriberId);
//     });

// module.exports = subscriberRouter;